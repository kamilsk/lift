module github.com/kamilsk/lift

go 1.13

require (
	github.com/pelletier/go-toml v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/afero v1.3.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.6.1
	go.octolab.org v0.0.29
	go.octolab.org/toolkit/cli v0.1.1
	go.octolab.org/toolkit/config v0.0.3
)
